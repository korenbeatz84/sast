# SAST Analyzers

## Overview

The SAST feature relies on underlying third party tools that are wrapped into what we call `Analyzers`.
An analyzer is a [dedicated project](https://gitlab.com/gitlab-org/security-products/analyzers) that wraps a particular tool to:
- expose its detection logic
- handle its execution
- convert its output to the common format

This is achieved by implementing the [common API](https://gitlab.com/gitlab-org/security-products/analyzers/common).

SAST currently supports the following official analyzers:

- [Bandit](https://gitlab.com/gitlab-org/security-products/analyzers/bandit)
- [Brakeman](https://gitlab.com/gitlab-org/security-products/analyzers/brakeman)
- [ESLint (Javascript)](https://gitlab.com/gitlab-org/security-products/analyzers/eslint)
- [SpotBugs with the Find Sec Bugs plugin (Ant, Gradle and wrapper, Grails, Maven and wrapper, SBT)](https://gitlab.com/gitlab-org/security-products/analyzers/spotbugs)
- [Flawfinder](https://gitlab.com/gitlab-org/security-products/analyzers/flawfinder)
- [Gosec](https://gitlab.com/gitlab-org/security-products/analyzers/gosec)
- [NodeJsScan](https://gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan)
- [PHP CS security-audit](https://gitlab.com/gitlab-org/security-products/analyzers/phpcs-security-audit)
- [Secrets (Gitleaks, TruffleHog & Diffence secret detectors)](https://gitlab.com/gitlab-org/security-products/analyzers/secrets)
- [Security Code Scan (.NET)](https://gitlab.com/gitlab-org/security-products/analyzers/security-code-scan)
- [TSLint (Typescript)](https://gitlab.com/gitlab-org/security-products/analyzers/tslint)
- [Sobelow (Elixir Phoenix)](https://gitlab.com/gitlab-org/security-products/analyzers/sobelow)

The Analyzers are published as Docker images that SAST will use to launch dedicated containers for each analysis.

SAST is pre-configured with a set of **default images** that are officially maintained by GitLab.

Users can also integrate their own **custom images**.

## Custom Analyzers

You can provide your own analyzers as a comma separated list of Docker images.
Here's how to add `analyzers/csharp` and `analyzers/perl` to the default images:

    SAST_ANALYZER_IMAGES=analyzers/csharp,analyzers/perl

Please note that this configuration doesn't benefit from the integrated detection step.
SAST has to fetch and spawn each Docker image to establish whether the custom analyzer can scan the source code.

### Writing Custom Analyzers

#### Prerequisites

To write a custom analyzer that will integrate into the GitLab application
a minimum number of features are required.

For integration into the GitLab product itself, the license must be evaluated.

#### Checklist

##### Underlying tool

 - [ ] Has permissive software license
 - [ ] Headless execution (CLI tool)
 - [ ] Executable using GitLab Runner's [Linux or Windows Docker executor](https://docs.gitlab.com/runner/executors/README.html#docker-executor)
 - [ ] Language identification method (file extension, package file, etc)

##### Minimal vulnerability data

 - [ ] name
 - [ ] description (helpful but not mandatory)
 - [ ] type (unique value to avoid collisions with other occurrences)
 - [ ] file path
 - [ ] line number

## Default analyzers

### Use a Docker mirror

You can switch to a custom Docker registry
that provides the official analyzer images under a different prefix.
For instance, the following instructs SAST to pull `my-docker-registry/gl-images/bandit`
instead of `registry.gitlab.com/gitlab-org/security-products/analyzers/bandit`:

    SAST_ANALYZER_IMAGE_PREFIX=my-docker-registry/gl-images

Please note that this configuration requires that your custom registry provides images for all the official analyzers.

### Select specific analyers

You can select the official analyzers you want to run.
Here's how to enable `bandit` and `flawfinder` while disabling all the other official analyzers:

    SAST_DEFAULT_ANALYZERS="bandit,flawfinder"

### Disable all analyzers

Setting `SAST_DEFAULT_ANALYZERS` to an empty string will disable all the default analyzers.

    SAST_DEFAULT_ANALYZERS=""

`SAST_DEFAULT_ANALYZERS_ENABLED` is now deprecated.

## Analyzers Data


| Property \ Tool                         | Bandit               | Brakeman             | ESLint security      | Find Sec Bugs        | Flawfinder           | Go AST Scanner       | NodeJsScan           | Php CS Security Audit   | Security code Scan (.NET)   | TSLint Security    | Sobelow            |
| --------------------------------------- | :------------------: | :------------------: | :------------------: | :------------------: | :------------------: | :------------------: | :------------------: | :---------------------: | :-------------------------: | :-------------:    | :----------------: |
| severity                                | :white_check_mark:   | :x:                  | :x:                  | :white_check_mark:   | :x:                  | :white_check_mark:   | :x:                  | :white_check_mark:      | :x:                         | :white_check_mark: | :x:                |
| title                                   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:      | :white_check_mark:          | :white_check_mark: | :white_check_mark: |
| description                             | :x:                  | :x:                  | :white_check_mark:   | :white_check_mark:   | :x:                  | :x:                  | :white_check_mark:   | :x:                     | :x:                         | :white_check_mark: | :white_check_mark: |
| file                                    | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:      | :white_check_mark:          | :white_check_mark: | :white_check_mark: |
| start line                              | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:      | :white_check_mark:          | :white_check_mark: | :white_check_mark: |
| end line                                | :white_check_mark:   | :x:                  | :white_check_mark:   | :white_check_mark:   | :x:                  | :x:                  | :x:                  | :x:                     | :x:                         | :white_check_mark: | :x:                |
| start column                            | :x:                  | :x:                  | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :x:                  | :white_check_mark:      | :white_check_mark:          | :white_check_mark: | :x:                |
| end column                              | :x:                  | :x:                  | :white_check_mark:   | :white_check_mark:   | :x:                  | :x:                  | :x:                  | :x:                     | :x:                         | :white_check_mark: | :x:                |
| external id (e.g. CVE)                  | :x:                  | :warning:            | :x:                  | :warning:            | :white_check_mark:   | :x:                  | :x:                  | :x:                     | :x:                         | :x:                | :x:                |
| urls                                    | :x:                  | :white_check_mark:   | :x:                  | :warning:            | :x:                  | :x:                  | :x:                  | :x:                     | :x:                         | :x:                | :x:                |
| internal doc/explanation                | :warning:            | :white_check_mark:   | :x:                  | :white_check_mark:   | :x:                  | :x:                  | :x:                  | :x:                     | :x:                         | :x:                | :white_check_mark: |
| solution                                | :x:                  | :x:                  | :x:                  | :warning:            | :white_check_mark:   | :x:                  | :x:                  | :x:                     | :x:                         | :x:                | :x:                |
| confidence                              | :white_check_mark:   | :white_check_mark:   | :x:                  | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :x:                  | :x:                     | :x:                         | :x:                | :white_check_mark: |
| affected item (e.g. class or package)   | :x:                  | :white_check_mark:   | :x:                  | :white_check_mark:   | :white_check_mark:   | :x:                  | :x:                  | :x:                     | :x:                         | :x:                | :x:                |
| source code extract                     | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :x:                  | :white_check_mark:   | :white_check_mark:   | :x:                  | :x:                     | :x:                         | :x:                | :x:                |
| internal id                             | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :white_check_mark:   | :x:                  | :white_check_mark:      | :white_check_mark:          | :white_check_mark: | :white_check_mark: |

- :white_check_mark: => we have that data
- :warning: => we have that data but it's partially reliable, or we need to extract it from unstructured content
- :x: => we don't have that data or it would need to develop specific or inefficient/unreliable logic to obtain it.

The values provided by these tools are heterogeneous so they are sometimes normalized into common values (e.g. `severity`, `confidence`, etc).
This mapping usually happens in the analyzer's `convert` command.
