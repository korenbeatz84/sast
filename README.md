# GitLab SAST

[![pipeline status](https://gitlab.com/gitlab-org/security-products/sast/badges/master/pipeline.svg)](https://gitlab.com/gitlab-org/security-products/sast/commits/master)
[![coverage report](https://gitlab.com/gitlab-org/security-products/sast/badges/master/coverage.svg)](https://gitlab.com/gitlab-org/security-products/sast/commits/master)

GitLab SAST performs Static Application Security Testing on given source code.
It's written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by SAST, Dependency Scanning and their analyzers.

## How to use

1. `cd` into the directory of the source code you want to scan
1. Run the Docker image:

    ```sh
    docker run \
      --interactive --tty --rm \
      --volume "$PWD":/code \
      --volume /var/run/docker.sock:/var/run/docker.sock \
      registry.gitlab.com/gitlab-org/security-products/sast:${VERSION:-latest} /app/bin/run /code
    ```

    `VERSION` can be replaced with the latest available release matching your GitLab version. See [Versioning](#versioning-and-release-process) for more details.

1. The results will be displayed and also stored in `gl-sast-report.json`

**Why mounting the Docker socket?**

SAST acts as a container orchestrator: it will pull and run analyzers (Docker images) based on the languages and frameworks detected.

## Settings

SAST can be configured using environment variables.

### Docker images

| Environment variable           | Function                                                                       |
|--------------------------------|--------------------------------------------------------------------------------|
| SAST_ANALYZER_IMAGES           | Comma separated list of custom images. Default images are still enabled.       |
| SAST_ANALYZER_IMAGE_PREFIX     | Override the name of the Docker registry providing the default images (proxy). |
| SAST_ANALYZER_IMAGE_TAG        | Override the Docker tag of the default images.                                 |
| SAST_DEFAULT_ANALYZERS         | Override the names of default images.                                          |
| SAST_PULL_ANALYZER_IMAGES      | Pull the images from the Docker registry (set to 0 to disable)                 |

Read more about [customizing analyzers](./docs/analyzers.md#custom-analyzers).

### Analyzer settings

Some analyzers can be customized with environment variables.

| Environment variable  | Analyzer | Function |
|-----------------------|----------|----------|
| ANT_HOME              | spotbugs | ANT_HOME environment variable. |
| ANT_PATH              | spotbugs | Path to the ant executable. |
| GRADLE_PATH           | spotbugs | Path to the gradle executable. |
| JAVA_OPTS             | spotbugs | Additional arguments for the java executable. |
| JAVA_PATH             | spotbugs | Path to the java executable. |
| MAVEN_CLI_OPTS        | spotbugs | Additional arguments for the mvn or mvnw executable. |
| MAVEN_PATH            | spotbugs | Path to the mvn executable. |
| MAVEN_REPO_PATH       | spotbugs | Path to the Maven local repository (shortcut for the maven.repo.local property). |
| SBT_PATH              | spotbugs | Path to the sbt executable. |
| FAIL_NEVER            | spotbugs | Set to `1` to ignore compilation failure |


### Vulnerability filters

Some analyzers make it possible to filter out vulnerabilities under a given threshold.

| Environment variable  | Default   | Function |
|-----------------------|-----------|----------|
| SAST_BANDIT_EXCLUDED_PATHS  |    | comma-separated list of paths to exclude from scan. Uses Python's [`fnmatch` syntax](https://docs.python.org/2/library/fnmatch.html) |
| SAST_BRAKEMAN_LEVEL   |         1 | Ignore Brakeman vulnerabilities under given confidence level. Integer, 1=Low 3=High. |
| SAST_FLAWFINDER_LEVEL |         1 | Ignore Flawfinder vulnerabilities under given risk level. Integer, 0=No risk, 5=High risk. |
| SAST_GITLEAKS_ENTROPY_LEVEL | 8.0 | Minimum entropy for secret detection. Float, 0.0 = low, 8.0 = high. |
| SAST_GOSEC_LEVEL      |         0 | Ignore gosec vulnerabilities under given confidence level. Integer, 0=Undefined, 1=Low, 1=Medium, 3=High. |
| SAST_EXCLUDED_PATHS   |           | Exclude vulnerabilities from output based on the paths. |

`SAST_EXCLUDED_PATHS` is a comma-separated list of patterns.
Patterns can be globs, file or folder paths. Parent directories will also match patterns.

### Timeouts

| Environment variable                   | Default | Function |
|----------------------------------------|---------|----------|
| SAST_DOCKER_CLIENT_NEGOTIATION_TIMEOUT |      2m | Time limit for Docker client negotiation |
| SAST_PULL_ANALYZER_IMAGE_TIMEOUT       |      5m | Time limit when pulling the image of an analyzer |
| SAST_RUN_ANALYZER_TIMEOUT              |     20m | Time limit when running an analyzer |

Timeouts are parsed using Go's [`ParseDuration`](https://golang.org/pkg/time/#ParseDuration).
Valid time units are "ns", "us" (or "µs"), "ms", "s", "m", "h".
Examples: "300ms", "1.5h" or "2h45m".

## Development

### Build project

Go 1.11 or higher is required to build SAST. Go modules must be enabled.

```sh
GO111MODULE=on go build -o sast
```

### Run locally

To run the command locally and perform the scan on `/tmp/code`:

```sh
CI_PROJECT_DIR=/tmp/code ./sast
```

### Integration tests

To run the integration tests:

```sh
./test.sh
```

## Supported languages, package managers and frameworks

The following table shows which languages, package managers and frameworks are supported and which tools are used.

| Language (package managers) / framework                                                                                                              | Scan tool                                                                                                                                                       | Introduced in GitLab Version                  |
|------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------------------------------------|
| .NET                                                                                                                                                 | [Security Code Scan](https://security-code-scan.github.io)                                                                                                      | 11.0                                          |
| Any                                                                                                                                                  | [Gitleaks](https://github.com/zricethezav/gitleaks), [TruffleHog](https://github.com/dxa4481/truffleHog) and [Diffence](https://github.com/techjacker/diffence) | 11.9                                          |
| C/C++                                                                                                                                                | [Flawfinder](https://www.dwheeler.com/flawfinder/)                                                                                                              | 10.7                                          |
| Elixir Phoenix                                                                                                                                       | [Sobelow](https://github.com/nccgroup/sobelow)                                                                                                                  | 11.10                                         |
| Go                                                                                                                                                   | [Gosec](https://github.com/securego/gosec)                                                                                                                      | 10.7                                          |
| Groovy ([Ant](https://ant.apache.org/), [Gradle](https://gradle.org/), [Maven](https://maven.apache.org/) and [SBT](https://www.scala-sbt.org/)[^1]) | [SpotBugs](https://spotbugs.github.io/) with the [find-sec-bugs](https://find-sec-bugs.github.io/) plugin                                                       | 11.3 (Gradle) & 11.9 (Ant, Maven, SBT)        |
| Java ([Ant](https://ant.apache.org/), [Gradle](https://gradle.org/), [Maven](https://maven.apache.org/) and [SBT](https://www.scala-sbt.org/)[^1])   | [SpotBugs](https://spotbugs.github.io/) with the [find-sec-bugs](https://find-sec-bugs.github.io/) plugin                                                       | 10.6 (Maven), 10.8 (Gradle) & 11.9 (Ant, SBT) |
| Javascript                                                                                                                                           | [ESLint security plugin](https://github.com/nodesecurity/eslint-plugin-security)                                                                                | 11.8                                          |
| Node.js                                                                                                                                              | [NodeJsScan](https://github.com/ajinabraham/NodeJsScan)                                                                                                         | 11.1                                          |
| PHP                                                                                                                                                  | [phpcs-security-audit](https://github.com/FloeDesignTechnologies/phpcs-security-audit)                                                                          | 10.8                                          |
| Python ([pip](https://pip.pypa.io/en/stable/))                                                                                                       | [bandit](https://github.com/openstack/bandit)                                                                                                                   | 10.3                                          |
| Ruby on Rails                                                                                                                                        | [brakeman](https://brakemanscanner.org)                                                                                                                         | 10.3                                          |
| Scala ([Ant](https://ant.apache.org/), [Gradle](https://gradle.org/), [Maven](https://maven.apache.org/) and [SBT](https://www.scala-sbt.org/)[^1])  | [SpotBugs](https://spotbugs.github.io/) with the [find-sec-bugs](https://find-sec-bugs.github.io/) plugin                                                       | 11.0 (SBT) & 11.9 (Ant, Gradle, Maven)        |
| Typescript                                                                                                                                           | [TSLint config security](https://github.com/webschik/tslint-config-security/)                                                                                   | 11.9                                          |

[^1]: And variants like the [Gradle wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html), [Grails](https://grails.org/) and the [Maven wrapper](https://github.com/takari/maven-wrapper). 

Tools are integrated into SAST via the analyzers, read the [documentation](./docs/analyzers.md) to know more about them.

## Versioning and release process

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

# Contributing

If you want to help and extend the list of supported scanners, read the
[contribution guidelines](CONTRIBUTING.md).
